#### 2.1. Finding a definition

Find where the `struct device` is implemented in the kernel code. Try using all these methods:

- `git grep`
- Ctags. For instance, with `vim -t`
- <https://elixir.bootlin.com>

#### 2.2. Getting metadata with git

Using git, find who did the v4.17, v4.18, v4.19 and v4.20 releases. Find also when these releases where made.
Do you see any pattern?

#### 2.3. Using git to learn about the Kernel

For each of these architectures:
- arm
- arm64
- x86
- mips

how many commits where made on the v4.20 release?

??? note "Hints"
    * A shell one-liner is more than enough to solve this.
    * See `git log --oneline`

#### 2.4. (Bonus) Find the first commit on your Linux kernel repo

#### 2.5. (Bonus) What percentage of the `init/main.c` file is kept intact since the file was first added to the kernel repo?

??? note "Hint"
    * See `git blame`

#### 2.6. Bisectability

Linux Kernel developers use the `git bisect` tool very often to find the root cause of kernel regressions. Moreover, the continuos integration system (<https://kernelci.org/>) recently added automatic bisection support.

Git bisection relies on an important property of the kernel repo: _bisectability_. This property simply means that the kernel will build, and boot on each and every git commit.

In other words, no commit is allowed to (at least knowingly!) break the build or the boot. The [kernel documentation](https://www.kernel.org/doc/html/latest/admin-guide/bug-bisect.html?highlight=bug) explains how git-bisect is used.

Assume you have a git topic branch with a couple dozen commits on top of the master branch. Can you think of a way to verify that the build is not breaking on any of the commits?

??? note "Hint"
    * See `git rebase --exec`

#### 2.7. Re-configuring the kernel

Find the `CONFIG_MULTIUSER` option. What are its dependencies? Disable it and rebuild the
kernel.

While the kernel is being rebuilt, find when the new option was introduced
and get the commit diffstat. Can you understand how it works, at least in a general sense?

#### 2.8. (Bonus) Making things optional

!!! Warning
    Make you sure you managed to understand the previous exercise before trying this one.

Following the spirit and design of the `CONFIG_MULTIUSER` _optionalization_, find where the `reboot` feature is implemented
and then make it optional, with a newly introduced option called `CONFIG_REBOOT`.

In other words, if you build a kernel with `CONFIG_REBOOT=N`, you won't be able to reboot or poweroff your kernel. Ever!
