# Main Structure

!!! summary "Goal"
    To level up people in linux kernel development.

!!! summary "How"
    With regular meetings with a certain schedule, with mini presentations and keeping track of our progress.

## The meetings will be organized in 3 main phases:

* **Phase 1) The basis of the kernel development.** The goal is to level the group ensuring that everyone acquires the basic skills to:

    * Interact with the community;
    * Compile and boot a kernel from source code;
    * Interpret logs and error messages;
    * Modify the code to add debug messages;
    * Implement a basic hello world module;
    * Understand the git workflow of working with patches;
    * Send patches to the community;

In addition, we should ensure that every participant is able to submit at least one patch to the kernel and is able to setup an optimized development environment.

* **Phase 2) Working on a specific project.** In this phase, students will choose a project to work on, some suggestions and guidance will be given but students will be free to choose the subject, work in groups or individually.

* **Phase 3) Spread the knowledge.** In this phase it will be incentivated to students to send talk proposals for conferences to present the work being done, to write blog posts about their work and to become mentors in programs as GSoC or Outreachy.

The main idea is to use this guide and make questions instead of having a teacher talking all the time. This way you will be able to follow your own pace and focus on the things you don't understand much.

**NOTE 1:** Newcomes will be always welcome in any point of the meetings. The material will be available for people to follow their on pace.

**NOTE 2:** This document may not provide all the information you require or have a too short description with pointers. Making question is incentivated. Check session about "Where/How to make questions".
